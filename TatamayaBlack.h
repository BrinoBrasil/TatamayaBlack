#ifndef TATAMAYABLACK_H
#define TATAMAYABLACK_H


#include <Arduino.h>

class TatamayaBlack{
    public:
        TatamyaBlack();
		void ligarLED(int LED);
		void desligarLED(int LED);
		void tocar(int freq);
		void tocar(int freq, int duration);
		void ligarRGB(int r, int g, int b);
		void ativarRGB();
		int leituraPotenciometro(int num);
		int leituraLuminosidade();
		const int LED_AMARELO = 6;
        const int LED_VERMELHO = 3;
        const int LED_AZUL = 2;
        const int LED_BRANCO = 11;
        const int LED_VERDE = 10;
        const int PINO_LED_R = 10;
        const int PINO_LED_G = 6;
        const int PINO_LED_B = 11;
        const int POT_1 = A2;
        const int POT_2 = A3;
        const int PINO_LDR = A1;
        const int PINO_BUZZER = 7;		
	private:
		
};
#endif
