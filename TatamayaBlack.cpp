#include "TatamayaBlack.h"
#include "Arduino.h"

void TatamayaBlack::ligarLED(int LED){
    digitalWrite(LED, HIGH);
}

void TatamayaBlack::desligarLED(int LED){
    digitalWrite(LED, LOW);
}

void TatamayaBlack::tocar(int freq){
    tone(PINO_BUZZER,freq);
}

void TatamayaBlack::tocar(int freq, int duration){
    tone(PINO_BUZZER, freq, duration);
}

void TatamayaBlack::ativarRGB(){
    pinMode(PINO_LED_R,OUTPUT);
    pinMode(PINO_LED_G,OUTPUT);
    pinMode(PINO_LED_B,OUTPUT);
}

void TatamayaBlack::ligarRGB(int r, int g, int b){
    analogWrite(PINO_LED_R, r);
    analogWrite(PINO_LED_G, g);
    analogWrite(PINO_LED_B, b);
}

int TatamayaBlack::leituraPotenciometro(int num){
    return analogRead(num);
}

int TatamayaBlack::leituraLuminosidade(){
    return analogRead(PINO_LDR);
}
				
